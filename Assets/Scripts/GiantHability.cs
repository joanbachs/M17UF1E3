﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantHability : MonoBehaviour
{
    public DataPlayer dataPlayer;
    private Vector3 maxScale;
    private Vector3 actualScale;

    bool ready = true;
    bool scaling = false;
    bool ungrowing = false;



    private enum HGStatus
    {
        idle,
        grow,
        giantState,
        ungrow,
        cooldown
    }

    private HGStatus currentState;

    private float timeRemaining = 6f;
    public float habilityCooldown = 10f;


    // Start is called before the first frame update
    void Start()
    {
        dataPlayer = GetComponentInParent<DataPlayer>();

        actualScale = transform.localScale;
        maxScale = actualScale + new Vector3(1, 1, 1) * (dataPlayer.height/10);

        currentState = HGStatus.idle;
    }

    // Update is called once per frame
    void Update()
    {

        

        switch (currentState)  //mirar enums google
        {
            case HGStatus.grow:
                {
                    growing();
                    break;
                }

            case HGStatus.giantState:
                {
                    giantState();
                    break;
                }

            case HGStatus.ungrow:
                {
                    ungrow();
                    break;
                }

            case HGStatus.cooldown:
                {
                    cooldown();
                    break;
                }
            case HGStatus.idle:
                {
                    idle();
                    break;
                }
        }

    }

    void growing()
    {
        if (ready)
        {
            ready = false;
            scaling = true;
        }

        if (scaling)
        {
            transform.localScale += new Vector3(1, 1, 1);

            if (transform.localScale.x >= maxScale.x)
            {
                scaling = false;
                currentState = HGStatus.giantState;
            }
        }
    }


    void giantState()
    {
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;

        }
        else
        {
            timeRemaining = 6f;
            currentState = HGStatus.ungrow;
        }

    }

    void ungrow()
    {
        ungrowing = true;

        if (ungrowing)
        {
            transform.localScale -= new Vector3(1, 1, 1);

            if (transform.localScale.x <= actualScale.x)
            {
                ungrowing = false;
                currentState = HGStatus.cooldown;
            }
        }
    }

    void cooldown()
    {
        if (habilityCooldown > 0)
        {
            habilityCooldown -= Time.deltaTime;

        }
        else
        {
            ready = true;
            habilityCooldown = 10f;
            currentState = HGStatus.idle;
        }
        
    }

    void idle()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            currentState = HGStatus.grow;
        }
    }
}
