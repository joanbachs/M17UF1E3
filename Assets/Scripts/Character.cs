﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public Transform playerTransform;

/*
    public string name;
    public string surname;
    public string characterType;
    public float height;
    public float weight;
    public float speed;
    public float distanceToCover;
*/

    public DataPlayer dataPlayer;
    
    public Sprite[] arraySprites = new Sprite[3];


    public bool isMoving;
    public float distanceToCoverFinishedX;
    public float distanceToCoverFinishedY;


    public SpriteRenderer spriteRenderer;

    int spriteIndex = 0;


    // Start is called before the first frame update
    void Start()
    {

        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = arraySprites[0];

        Application.targetFrameRate = 12;

        distanceToCoverFinishedX = 0;
        distanceToCoverFinishedY = 0;

    }

    // Update is called once per frame
    void Update()
    {

        if (isMoving == true)
        {
            spriteRenderer.sprite = arraySprites[spriteIndex];

            if (spriteIndex == 2){
                spriteIndex = 1;
            }
            else
            {
                spriteIndex++;
            }
        }
        else
        {
            spriteRenderer.sprite = arraySprites[0];
        }


        if (distanceToCoverFinishedX >= dataPlayer.distanceToCover)
        {
            spriteRenderer.flipX = true;
            spriteRenderer.sprite = arraySprites[0];
        }
        else
        {
            transform.position = new Vector3(transform.position.x - dataPlayer.speed, transform.position.y);
            distanceToCoverFinishedX += dataPlayer.speed;
        }


    }
}
