﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ScriptUI : MonoBehaviour
{
    public Text VidesText;
    public Text ScoreText;
    public Text NumEnemiesText;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        VidesText.text = "Lifes: " + GameManager.Instance.Player.GetComponent<DataPlayer>().lives;

        ScoreText.text = "Score: " + GameManager.Instance.Score;

        NumEnemiesText.text = ""+GameManager.Instance.numEnemiesOnScene;
    }
}
