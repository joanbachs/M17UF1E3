﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class DeleteEnemy : MonoBehaviour
{
    [SerializeField]
    private int scorePoints = 5;
    public void OnMouseDown()
    {
        Destroy(this.gameObject);
        GameManager.Instance.IncreaseScore(scorePoints);
        GameManager.Instance.refreshEnemiesNumber(-1);
    }
}
