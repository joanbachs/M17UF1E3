﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    public Transform playerTransform;
    public DataPlayer dataPlayer;

    public Sprite[] arraySprites = new Sprite[3];


    public bool isMoving;
    public float distanceToCoverFinishedX;
    public float distanceToCoverFinishedY;

    public SpriteRenderer spriteRenderer;

    int spriteIndex = 0;

    private bool canviDireccio = true;


    // Start is called before the first frame update
    void Start()
    {

        dataPlayer = gameObject.GetComponent<DataPlayer>();


        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = arraySprites[0];

        Application.targetFrameRate = 12;

        distanceToCoverFinishedX = 0;
        distanceToCoverFinishedY = 0;


        canviDireccio = true;

        
        if (dataPlayer.weight <= 10f)
        {
            dataPlayer.speed = 0.8f;
        }
        else if (dataPlayer.weight <= 25f)
        {
            dataPlayer.speed = 0.4f;
        }
        else if (dataPlayer.weight <= 40f)
        {
            dataPlayer.speed = 0.1f;
        }
        else
        {
            dataPlayer.speed = 0.01f;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (canviDireccio)
        {
            WalkToLeft();
        }
        else
        {
            WalkToRight();
        }
    }


    void WalkToLeft()
    {
        if (isMoving == true)
        {
            
            spriteRenderer.sprite = arraySprites[spriteIndex];

            if (spriteIndex == 2)
            {
                spriteIndex = 1;
            }
            else
            {
                spriteIndex++;
            }

            if (distanceToCoverFinishedX >= dataPlayer.distanceToCover)
            {
                spriteRenderer.flipX = true;
                spriteRenderer.sprite = arraySprites[0];
                canviDireccio = false;

                distanceToCoverFinishedX = 0;
            }
            else
            {
                transform.position = new Vector3(transform.position.x - dataPlayer.speed, transform.position.y);
                distanceToCoverFinishedX += dataPlayer.speed;
            }
        }
        else
        {
            Debug.Log(spriteRenderer.sprite);
            spriteRenderer.sprite = arraySprites[0];
        }
    }

    void WalkToRight()
    {
        if (isMoving == true)
        {
            spriteRenderer.sprite = arraySprites[spriteIndex];

            if (spriteIndex == 2)
            {
                spriteIndex = 1;
            }
            else
            {
                spriteIndex++;
            }

            if (distanceToCoverFinishedY >= dataPlayer.distanceToCover)
            {
                spriteRenderer.flipX = false;
                spriteRenderer.sprite = arraySprites[0];
                canviDireccio = true;

                distanceToCoverFinishedY = 0;
            }
            else
            {
                transform.position = new Vector3(transform.position.x + dataPlayer.speed, transform.position.y);
                distanceToCoverFinishedY += dataPlayer.speed;
            }
        }
        else
        {
            spriteRenderer.sprite = arraySprites[0];
        }
    }

}
