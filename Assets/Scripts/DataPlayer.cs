﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerKind
{
    Sorcerer,
    Knight,
    Assassin,
    Bard,
    Barbarian
}

public class DataPlayer : MonoBehaviour
{
    [SerializeField]
    public string name;
    public string surname;
    public float height;
    public float weight;
    public float speed;
    public float distanceToCover;
    public int lives;

    public PlayerKind Kind;


    // Start is called before the first frame update
    void Start()
    {
        lives = 2;
    }

    // Update is called once per frame
    void Update()
    {

    }

   
}
