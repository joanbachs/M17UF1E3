﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class EnemyCollitionController : MonoBehaviour
{
    //Collition controller with enemies and player

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Sóc l'enemic i he col·lisionat");

        if (collision.CompareTag("Player"))
        {
            //Code collition with player
            Debug.Log("Colisió amb el player");

            //Aplicar dmg
            GameObject pl = collision.gameObject;

            pl.GetComponent<DataPlayer>().lives--;

            if (pl.GetComponent<DataPlayer>().lives <= 0)
            {
                Destroy(pl);
                new WaitForSeconds(2);
                LoadCurrentScene();
            }
        }
        Destroy(gameObject);
        GameManager.Instance.refreshEnemiesNumber(-1);

    }

    public void LoadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
